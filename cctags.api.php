<?php

/**
 * @file
 * Hooks for the cctags module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the term link.
 *
 * @param object $erm
 *   Taxonomy term
 * @param array $options
 *   Array of link options.
 */
function hook_cctags_more_alter($erm, &$options) {
  $options['attributes']['data-term-id'] = $erm->tid;
}

/**
 * @} End of "addtogroup hooks".
 */
